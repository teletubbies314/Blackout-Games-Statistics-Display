import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import Graph from './Graph.js';
import Elem from './Elem.js';
import App from './App.js';
import GraphDropdown from './GraphDropdown.js';
import LatencyGraph from './LatencyGraph.js';
import TimespanDropdown from './TimespanDropdown.js';
import openSocket from 'socket.io-client';
import Stats from './Stats.js';
ReactDOM.render(<App renderFunc={updateState} />, document.getElementById('root'));

var currentGraph = {val: 'online', interval: 'hourly', limit: '25', timespan: '1DAY',};

var currentState = 0;

var LATENCY = [];
var DATA = [];
var status = {};
const socket = openSocket('https://io.blackoutrugby.com:8080');
socket.on(`statusUpdates`, function(payload){
	console.log(`statusUpdates event received`, payload);
	if(currentState == 0) {
		//ReactDOM.render(<Stats socket={socket}/>, document.getElementById('widget2'));
		updateState(0);
	}
});

function updateState(i) {
	currentState = i;
	ReactDOM.render(<Elem state={i} socket={socket}/>, document.getElementById('widget2'));
}

updateState(0);
//updateState(1);
//updateState(2);
//updateState(0);
registerServiceWorker();
