import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Graph from './Graph.js';
import './App.css';


export default class GraphDropdown extends Component { 
  
  constructor(props) {
    super(props);
    this.renderGraph = this.renderGraph.bind(this);
  }

  renderGraph(event) {
  	this.props.renderFunc(event, false, false, false);
  }

  render() {
    return (
	<div className="gdropdown">
		<div className="gdropdown-button">
			<a href="#">Graph Type</a>
		</div>
		<div className="gdropdown-content">
			<a href="#"><div onClick={() => {this.renderGraph('dau')}}>DAU</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('mau')}}>MAU</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('online')}}>ONLINE</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('installs')}}>INSTALLS</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('signups')}}>SIGNUPS</div></a>
		</div>
	</div>
    );
  }
}
