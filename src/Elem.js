import Graph from './Graph.js';
import Stats from './Stats.js';
import LatencyGraph from './LatencyGraph.js';
import React, { Component } from 'react';

export default class Elem extends Component {

  render() {
  var sock = this.props.socket;
  var state = this.props.state;
  if(state === 0) {
    return (
	<Stats socket={sock}/>
    );
  } else if (state === 1) {
    return (
	<LatencyGraph socket={sock}/>
    );
  } else if (state === 2) {
    return (
   	<Graph socket={sock}/>
    );
  }
  return null;
  }
}
