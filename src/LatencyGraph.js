import ReactDOM from 'react-dom';
import React, { Component} from 'react';
import {FlexibleXYPlot, XAxis, YAxis, HorizontalGridLines, VerticalGridLines, LineMarkSeries, AreaSeries, DiscreteColorLegend} from 'react-vis';
import './Graph.css';

export default class LatencyGraph extends Component {
  constructor(props) {
    super(props);
    this.maxY = 10;
    this.latlistlen = 0;
    this.state = { dataOne: [], dataTen: [], dataFive: [] };
  }

guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

  tick() {
    this.setState(prevState=> ({ seconds: prevState.seconds + 1 }));
    const payload = {
        method: 'get',
	path: 'status',
	header: {},
	requestId: this.guid()
    }
    this.props.socket.emit(`api`, payload, function(reply) {
	this.parseLatency(reply);
    }.bind(this));
  }

  parseLatency(data) {
    var a = JSON.parse(JSON.stringify(data));
    var j = JSON.parse(a.body);
    var i = j.data[12].attributes;
    this.updateLatency(i.last10s, i.last1m, i.last5m);
    this.updateAverage(i.last10s, i.last1m, i.last5m);
 }
  updateAverage(tens, onem, fivem) {
	/*ReactDOM.render(<span>AVERAGE LATENCY</span>, document.getElementById('statstext'));
	ReactDOM.render(<span>Average 10s: {tens} <br/>
                Average 1m: {onem}  <br/>
                Average 5m: {fivem}
		</span>, document.getElementById('staty'));
	*/
	ReactDOM.render(<div>
	<div className="statstext">
		AVERAGE LATENCY
	</div>
	<div className="statstext2" id="staty">
		Average 10s: {tens} <br/>
		Average 1m: {onem}  <br/>
		Average 5m: {fivem}
	</div>
</div>, document.getElementById("widget1"));
  }
  updateLatency(tens, onem, fivem) {
	if(tens + 5> this.maxY) { this.maxY = tens + 5 };
        if(onem + 5 > this.maxY) { this.maxY = onem + 5};
        if(fivem + 5 > this.maxY) { this.maxY = fivem + 5};
        this.state.dataTen.unshift({x: -1, y: tens});
        this.state.dataOne.unshift({x: -1, y: onem});
        this.state.dataFive.unshift({x: -1, y: fivem});
	if(this.state.dataTen.length > 60) {
		this.state.dataTen.length = 60;
		this.state.dataOne.length = 60;
		this.state.dataFive.length = 60;
	}
	var i;
	for(i = 0; i < this.state.dataTen.length; i++) {
		this.state.dataTen[i].x++;
		this.state.dataOne[i].x++;
		this.state.dataFive[i].x++;
	}
  }

  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    let xDomain = [59, 0];
    let yDomain = [0, this.maxY];
    let legendItems = [{title: 'Last 10s', color: '#4CC', strokeWidth:3}, {title: 'Last 5m', color: '#CC4', strokeWidth:3},{title: 'Last 1m', color: '#C4C', strokeWidth:3}];
    return (
	<div>
		<div className="title" id="graphTitle">Latency</div>
		<div className="content">
		<div className="Graph">
			<FlexibleXYPlot xType="linear" xDomain={xDomain} yDomain={yDomain}>
				<VerticalGridLines style={{stroke: 'white', opacity:0.2}}/>
				<HorizontalGridLines style={{stroke: 'white', opacity:0.2}}/>
				<XAxis position='middle' /*tickValues={xValues} tickFormat={xFormat}*/ style={{line: {stroke: "none"}, ticks: {stroke: "#FFF"}, text: {stroke: 'none', fill: '#FFF'}}}/>
				<YAxis position='middle' style={{line: {stroke: "none"}, ticks: {stroke: "#FFF"}, text: {stroke: 'none', fill: '#FFF'}}}/>
				<LineMarkSeries
				  data={this.state.dataTen}
				  //curve={'curveMonotoneX'}
				  markStyle={{fill: "none", stroke: "none"}}
				  lineStyle={{fill: "none", stroke: '4CC', strokeWidth:3}}
				/>
                                <LineMarkSeries
                                  data={this.state.dataFive}
                                  //curve={'curveMonotoneX'}
                                  markStyle={{fill: "none", stroke: "none"}}
                                  lineStyle={{fill: "none", stroke: 'CC4', strokeWidth:3}}
                                />
                                <LineMarkSeries
                                  data={this.state.dataOne}
                                  //curve={'curveMonotoneX'}
                                  markStyle={{fill: "none", stroke: "none"}}
                                  lineStyle={{fill: "none", stroke: 'C4C', strokeWidth:3}}
                                />
			</FlexibleXYPlot>
			<div>
				<br/>
				<span style={{padding: '30px', color:'#4cc'}}><b>10 Seconds</b></span>
				<span style={{padding: '30px', color:'#c4c'}}><b>1 Minute</b></span>
				<span style={{padding: '30px', color:'#cc4'}}><b>5 Minutes</b></span>
			</div>
		</div>
		</div>
	</div>
    );
  }
}
