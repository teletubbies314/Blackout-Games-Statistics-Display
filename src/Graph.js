import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import {FlexibleXYPlot, XAxis, YAxis, HorizontalGridLines, VerticalGridLines, LineMarkSeries, AreaSeries} from 'react-vis';
import './Graph.css';
import TimespanDropdown from './TimespanDropdown.js';
import GraphDropdown from './GraphDropdown.js';
import PlatformDropdown from './PlatformDropdown.js';

var GRAPH_DATA = [{x:0,y:0}];

var currentGraph = {val: 'dau', interval: 'hourly', limit: '25', timespan: '1DAY', platform: 'windows'};
var graphTitle = "Daily Active Users";

function guid() {
	function s4() {
	return Math.floor((1 + Math.random()) * 0x10000)
		.toString(16)
		.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

const ONE_HOUR = 3600000;
const ONE_DAY = ONE_HOUR * 24;
var TIMESTAMP;

// Formatter for X values for rendering anything over a day
function GenericFormat(value) {
	var retVal = (TIMESTAMP - value) / ONE_DAY;
	if(retVal !== 0)
		retVal = "-" + retVal;
	retVal += "d";
	return retVal;
}

// Formatter for X values for rendering anything less than a days worth
function GenericDayFormat(value) {
	var retVal = (TIMESTAMP - value) / ONE_HOUR;
	if(retVal !== 0)
		retVal = "-" + retVal;
	retVal += "h";
	return retVal;
}

export default class Graph extends Component {

componentDidMount() {
	this.updateGraph(false, false, false, false);
}

constructor(props) {
	super(props);
	this.componentDidMount = this.componentDidMount.bind(this);
	this.forceUpdate = this.forceUpdate.bind(this);
	this.updateGraph = this.updateGraph.bind(this);
	this.getHistory = this.getHistory.bind(this);
}

getHistory(socket) {
        const payload = {
                method: 'get',
                path: 'status-history?interval=' + currentGraph.interval + '&limit=' + currentGraph.limit,
                headers: {},
                requestId: guid()
        }
        socket.emit(`api`, payload, function(reply) {
                //this.parseHistory(currentGraph.val, reply);
		//if(currentGraph.val !== 'signups') {
		//	this.parseByPlatform(currentGraph.val, reply);
		//} else {
			this.parseHistory(currentGraph.val, reply);
		//}
        }.bind(this));
}

parseHistory(val, data) {
	console.log(data);
        var temp = [];
        var a = JSON.parse(JSON.stringify(data));
        var j = JSON.parse(a.body);
        var i;
        for(i=0;i < j.length;i++){
                var d = new Date(j[i].date);
                temp.push({x: d.getTime(), y: j[i].stats[val]});
        }
	GRAPH_DATA = temp;
	this.forceUpdate();
}

parseByPlatform(val, data) {
	val += '-platforms';
        console.log("parsebyplatform");
        var temp = [];
        var a = JSON.parse(JSON.stringify(data));
        var j = JSON.parse(a.body);
        var i;
        for(i=0;i < j.length;i++){
                var d = new Date(j[i].date);
                temp.push({x: d.getTime(), y: j[i].stats[val][currentGraph.platform]});
        	console.log(temp);
	}
        GRAPH_DATA = temp;
        this.forceUpdate();

}

updateGraph(val, interval, limit, timespan, platform) {
        if(val) {
                currentGraph.val = val;
		if(val === 'dau') {
			graphTitle = "Daily Active Users";
		} else if (val === 'online') {
			graphTitle = "Online Users";
		} else if (val === 'mau') {
			graphTitle = "Monthly Active Users";
		} else if (val === 'signups') {
			graphTitle = "Signups";
		} else if (val === 'installs') {
			graphTitle = "Installs";
		}
        }
        if(interval) {
                currentGraph.interval = interval;
        }
        if(limit) {
                currentGraph.limit = limit;
        }
        if(timespan) {
                currentGraph.timespan = timespan;
        }
	if(platform) {
		currentGraph.platform = platform;
	}
	this.getHistory(this.props.socket);
}

  render() {
		// Variables
		const domain = currentGraph.timespan;
		let data = GRAPH_DATA;
		let xFormat, xDomain, yDomain;
		let xValues = [];
		TIMESTAMP = data[data.length-1].x;
		yDomain = [0,1]

		// Get max Y value
		var i;
		for(i = 0; i < data.length; i++) {
			if(data[i].y > yDomain[1]) {
				yDomain[1] = data[i].y;
			}
		}

		// Get Y domain, round up max Y value
		if(yDomain[1] % 5 === 0) {
			yDomain[1] += 5;
		} else {
			yDomain[1] = Math.ceil(yDomain[1] / 5) * 5;
		}

		// Check which domain we are going to render the graph to
		if (domain === "1DAY") {
			xDomain = [TIMESTAMP - ONE_DAY, TIMESTAMP];
			xFormat = GenericDayFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= 12 * ONE_HOUR) {
				xValues.push(i);
			}
			//data = data.slice(Math.max(data.length - 1, 25));
			for(i = 0; i < data.length; i++) {
				if(data[i].x >= TIMESTAMP - ONE_DAY) {
					data = data.slice(i);
					break;
				}
			}
		} else if (domain === "1WEEK") {
			xDomain = [TIMESTAMP - ONE_DAY * 7, TIMESTAMP];
			//data = data.slice(Math.max(data.length - 8));
			xFormat = GenericFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= ONE_DAY) {
				xValues.push(i);
			}
			for(i = 0; i < data.length; i++) {
				if(data[i].x >= TIMESTAMP - ONE_DAY * 7) {
					data = data.slice(i);
					break;
				}
			}
		} else if (domain === "1MONTH") {
			xDomain = [TIMESTAMP - ONE_DAY * 28, TIMESTAMP];
			//data = data.slice(Math.max(data.length - 29));
			xFormat = GenericFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= 7*ONE_DAY) {
				xValues.push(i);
			}
                        for(i = 0; i < data.length; i++) {
                                if(data[i].x >= TIMESTAMP - ONE_DAY * 28) {
                                        data = data.slice(i);
                                        break;
                                }
                        }
		} else if (domain === "3MONTH") {
			xDomain = [TIMESTAMP - ONE_DAY * 90, TIMESTAMP];
			xFormat = GenericFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= 10*ONE_DAY) {
				xValues.push(i);
			}
                        for(i = 0; i < data.length; i++) {
                                if(data[i].x >= TIMESTAMP - ONE_DAY * 90) {
                                        data = data.slice(i);
                                        break;
                                }
                        }

			//data = data.slice(Math.max(data.length - 91));
		} else if (domain === "6MONTH") {
			xDomain = [TIMESTAMP - ONE_DAY * 180, TIMESTAMP];
			//data = data.slice(Math.max(data.length - 181));
			xFormat = GenericFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= 30*ONE_DAY) {
				xValues.push(i);
			}
                        for(i = 0; i < data.length; i++) {
                                if(data[i].x >= TIMESTAMP - ONE_DAY * 180) {
                                        data = data.slice(i);
                                        break;
                                }
                        }

		} else if (domain === "1YEAR") {
			xDomain = [TIMESTAMP - ONE_DAY * 360, TIMESTAMP];
			//data = data.slice(Math.max(data.length - 366));
			xFormat = GenericFormat;
			for(i = xDomain[0]; i <= xDomain[1]; i+= 60*ONE_DAY) {
				xValues.push(i);
			}
                        for(i = 0; i < data.length; i++) {
                                if(data[i].x >= TIMESTAMP - ONE_DAY * 360) {
                                        data = data.slice(i);
                                        break;
                                }
                        }

		} else {
			// Did not recognize domain format
		}
    return (
	<div>
		<div className="title" id="graphTitle">{graphTitle}</div>
                <div className="dropdowncontainer">
                	<div id="graphDropdown"><GraphDropdown renderFunc={this.updateGraph}/></div>
                	<div id="timespanDropdown"><TimespanDropdown renderFunc={this.updateGraph}/></div>
                </div>
		<div className="content">
			<div className="Graph" id="mainGraph">
				<FlexibleXYPlot xType="time" xDomain={xDomain} yDomain={yDomain}>
					<VerticalGridLines style={{stroke: 'white', opacity:0.2}}/>
					<HorizontalGridLines style={{stroke: 'white', opacity:0.2}}/>
					<XAxis position='middle' tickValues={xValues} tickFormat={xFormat} style={{line: {stroke: "none"}, ticks: {stroke: "#FFF"}, text: {stroke: 'none', fill: '#FFF'}}}/>
					<YAxis position='middle' style={{line: {stroke: "none"}, ticks: {stroke: "#FFF"}, text: {stroke: 'none', fill: '#FFF'}}}/>
					<AreaSeries
					  opacity={0.3}
					  data={data}
					  //curve={'curveMonotoneX'}
					  lineStyle={{fill: "none", stroke: '#4CC', strokeWidth:2}}
					/>
					<LineMarkSeries
					  data={data}
					  //curve={'curveMonotoneX'}
					  markStyle={{fill: "none", stroke: "none"}}
					  lineStyle={{fill: "none", stroke: '4CC', strokeWidth:3}}
					/>
				</FlexibleXYPlot>
			</div>
		</div>
    	</div>
    );
  }
}
