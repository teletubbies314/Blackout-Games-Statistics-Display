import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Graph from './Graph.js';
import './App.css';


export default class TimespanDropdown extends Component { 
  
  constructor(props) {
    super(props);

    this.renderGraph = this.renderGraph.bind(this);
  }

  renderGraph(event) {
	var val;
	var interval = false;
	if(event === '1DAY') {
		val = 25;
		interval = 'hourly';
	} else if (event === '1WEEK') {
		val = 1 + (7 * 24);
		interval = 'hourly'
	} else if (event === '1MONTH') {
		val = 28;
		interval = 'daily';
	} else if (event === '3MONTH') {
		val = 90;
		interval = 'daily';
	} else if (event === '6MONTH') {
		val = 180;
		interval = 'daily';
	} else if (event === '1YEAR') {
		val = 360;
		interval = 'daily'
	}
  	this.props.renderFunc(false, interval, val, event);
  }

  render() {
    return (
	<div className="tdropdown">
		<div className="gdropdown-button">
			<a href="#">Granularity</a>
		</div>
		<div className="gdropdown-content">
			<a href="#"><div onClick={() => {this.renderGraph('1DAY')}}>1 Day</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('1WEEK')}}>1 Week</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('1MONTH')}}>1 Month</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('3MONTH')}}>3 Months</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('6MONTH')}}>6 Months</div></a>
			<a href="#"><div onClick={() => {this.renderGraph('1YEAR')}}>1 Year</div></a>
		</div>
	</div>
    );
  }
}
