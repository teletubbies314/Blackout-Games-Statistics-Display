import ReactDOM from 'react-dom';
import React, { Component } from 'react';
import openSocket from 'socket.io-client';

let status = {};
let currentStat = 'dau';

function guid() {
        function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

export default class Stats extends Component {
	constructor(props) {
		super(props);
		this.forceUpdate = this.forceUpdate.bind(this);
		this.componentDidMount = this.componentDidMount.bind(this);
		this.componentDidUpdate = this.componentDidUpdate.bind(this);
	}

	componentDidUpdate() {
		this.getStatus();
	}

	componentDidMount() {
		this.getStatus();
	}

	updateRealTimeDisplay(val) {
		currentStat = val;
		if(val === 'signups') {
			ReactDOM.render(<span>Signups</span>, document.getElementById('statstext'));
			ReactDOM.render(<span>{status.signups}</span>, document.getElementById('staty'));
		}
		else if(val === 'mau') {
			ReactDOM.render(<span>Monthly Active Users</span>, document.getElementById('statstext'));
			ReactDOM.render(<span>{status.mau}</span>, document.getElementById('staty'));
		}
		else if(val === 'dau') {
		        ReactDOM.render(<span>Daily Active Users</span>, document.getElementById('statstext'));
			ReactDOM.render(<span>{status.dau}</span>, document.getElementById('staty'));
		}
		else if(val === 'installs') {
	                ReactDOM.render(<span>Installs</span>, document.getElementById('statstext'));
			ReactDOM.render(<span>{status.installs}</span>, document.getElementById('staty'));
		}
		else if(val === 'online') {
			ReactDOM.render(<span>Online</span>, document.getElementById('statstext'));
			ReactDOM.render(<span>{status.online}</span>, document.getElementById('staty'));
		}
	}

        getStatus() {
                const payload = {
                        method: 'get',
                        path: 'status',
                        headers: {},
                        requestId: guid()
                }
                this.props.socket.emit(`api`, payload, function(reply) {
			this.parse(reply);
		}.bind(this));
	}


        parse(data) {
                var a = JSON.parse(JSON.stringify(data));
                var j = JSON.parse(a.body);
                var i;
                for(i = 0; i < 5; i++){
                        var id = j.data[i].id;
                        var value = j.data[i].attributes.value;
                        status[id] = value;
                }
		this.updateRealTimeDisplay(currentStat);
		this.forceUpdate();
        }

  render() {
    return (
	<div>
		<div className="title" id="graphTitle">Statistics</div>
        	<div className="content">
			<div className="statcontainer">
				<span className="statlist" onClick={() => {this.updateRealTimeDisplay('dau')}}>Daily Active Users: {status.dau}</span><br/>
	           	    	<span className="statlist" onClick={() => {this.updateRealTimeDisplay('mau')}}>Monthly Active Users: {status.mau}</span><br/>
        	       		<span className="statlist" onClick={() => {this.updateRealTimeDisplay('installs')}}>Installs: {status.installs}</span><br/>
               			<span className="statlist" onClick={() => {this.updateRealTimeDisplay('online')}}>Online: {status.online}</span><br/>
               			<span className="statlist" onClick={() => {this.updateRealTimeDisplay('signups')}}>Signups: {status.signups}</span>
			</div>
		</div>
	</div>
    );
  }
}
