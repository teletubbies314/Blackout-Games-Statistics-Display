import React, { Component } from 'react';
import './App.css';
import './Graph.css';
import logo from './images/Blackout-logo.png';
import rugby from './images/rugby-active.png';
import cricket from './images/cricket.png';
import football from './images/football.png';
class App extends Component {
  constructor(props){
	super(props)
	this.state={
		showRealtime:true,
		showLatency:false,
		showHistoric:false
	}
  }
  switchelements(i){
	  if(i == 0){
		  if(!this.state.showRealtime){
			  this.setState({
				  showRealtime:true,
				  showLatency:false,
				  showHistoric:false
			  })
		  }
	  }
	  else if(i == 1){
		  if(!this.state.showLatency){
			  this.setState({
				  showRealtime:false,
				  showLatency:true,
				  showHistoric:false
			  })
		  }
	  }
	  else if(i == 2){
		  if(!this.state.showHistoric){
			  this.setState({
				  showRealtime:false,
				  showLatency:false,
				  showHistoric:true
			  })
		  }
	  }
	this.props.renderFunc(i);
	var j, tablinks;
	tablinks = document.getElementsByClassName("tablink");
	for (j = 0; j < tablinks.length; j++) {
		tablinks[j].className = tablinks[j].className.replace(" active", "");
	}
	tablinks[i].className += " active";


  }

  render() {
    return (
		<div className="App">
			<div className="side-nav">
				<div className="logo">
                                	<img src={logo} height="auto" width="100%"/>
				</div>
				<nav>
					<ul>
						<li>
							<a className="tablink active" href="#" onClick={()=>this.switchelements(0)}>
								<span>Real-time</span>
							</a>
						</li>
						<li>
							<a className="tablink" href="#" onClick={()=>this.switchelements(1)}>
								<span>Latency</span>
							</a>
						</li>
						<li>
							<a className="tablink" href="#" onClick={()=>this.switchelements(2)}>
								<span>Historic Data</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
			<div className="main-content">
				<div className="header">
                                        <img src={rugby} id="active" height="44px" width="auto"/>
                                        <img src={cricket} height="44px" width="auto"/>
                                        <img src={football} height="44px" width="auto"/>
                                </div>
				<div className="main" id="main">
					{
						this.state.showRealtime?
						<div className="fullwidthwidget" id="widget1">
							<div className="statstext" id="statstext">
								REAL-TIME
							</div>
							<div className="stat" id="staty">
                                                        </div>
						</div>
						:null
					}
					{
						this.state.showLatency?
						<div className="fullwidthwidget" id="widget1">
							<div className="statstext">
								AVERAGE LATENCY
							</div>
								<div className="statstext2" id="staty">
									Average 10s: 0 <br/>
									Average 1m: 0<br/>
									Average 5m: 0
								</div>
						</div>
						:null
					}
					{
						this.state.showHistoric?
						null
						:null
					}
					<div className="fullwidthwidget" id="widget2">
					</div>
				</div>
			</div>
		</div>
    );
  }
}

export default App;
